package app

/*
Copyright © 2020 Colin Fox <greenenergy@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"

	"github.com/google/uuid"

	"gitlab.com/greenenergy/todo/pkg/logs"

	"github.com/spf13/viper"
	pb "gitlab.com/greenenergy/todo/service/todo"
	"google.golang.org/grpc"
)

// Persistor - interface definition for the persistence layer.
type Persistor interface {
	Add(*pb.TodoDef) error
	Delete(*pb.TodoDef) error
	Update(*pb.TodoDef) (*pb.TodoDef, error)
	Get(*pb.TodoDef) (*pb.TodoDef, error)
	List(*pb.TodoListReq) (*pb.TodoList, error)
}

// Listener -
type Listener struct {
	id  string
	out chan pb.Event
}

// TodoServer - main control structure for the todo server
type TodoServer struct {
	// redis connection
	config           *viper.Viper
	persistenceLayer Persistor
	listeners        map[string]*Listener
	sync.Mutex
}

func (ts *TodoServer) addListener(l *Listener) {
	ts.Lock()
	ts.listeners[l.id] = l
	ts.Unlock()
}

func (ts *TodoServer) delListener(l *Listener) {
	ts.Lock()
	delete(ts.listeners, l.id)
	ts.Unlock()
}

// NewTodoServer - allocate a new Todo server.
func NewTodoServer(config *viper.Viper, p Persistor) *TodoServer {
	return &TodoServer{
		config:           config,
		persistenceLayer: p,
		listeners:        make(map[string]*Listener),
	}
}

func (ts *TodoServer) broadcast(action string, td *pb.TodoDef) {
	e := pb.Event{
		Action: action,
		Todo:   td,
	}
	tmps, _ := json.MarshalIndent(e, "", "    ")
	fmt.Println("BCAST:", string(tmps))

	ts.Lock()
	for _, l := range ts.listeners {
		l.out <- e
	}
	ts.Unlock()
}

// Add - Add handler function for Todo Server
func (ts *TodoServer) Add(ctx context.Context, td *pb.TodoDef) (*pb.TodoDef, error) {
	td.Id = uuid.New().String()

	tmps, _ := json.MarshalIndent(td, "", "    ")
	logs.Debug.Println("asked to add this:", string(tmps))

	err := ts.persistenceLayer.Add(td)
	ts.broadcast("add", td)
	return td, err
}

// Update -
func (ts *TodoServer) Update(ctx context.Context, td *pb.TodoDef) (*pb.TodoDef, error) {
	// Here we could report either the incoming update request or the final result.
	// Based on the idea of monitoring changes, it seems to make sense to report the incoming
	// request instead. However, if the final result is desired instead, just broadcast the
	// result rather than td
	ts.broadcast("update", td)
	result, err := ts.persistenceLayer.Update(td)
	return result, err
}

// Delete -
func (ts *TodoServer) Delete(ctx context.Context, td *pb.TodoDef) (*pb.TodoDef, error) {
	ts.broadcast("delete", td)
	err := ts.persistenceLayer.Delete(td)
	return td, err
}

// ListTodos -
func (ts *TodoServer) ListTodos(ctx context.Context, tlr *pb.TodoListReq) (*pb.TodoList, error) {
	// If a username is provided, then we list only that user's todos.
	// Otherwise we list them all.
	faketd := pb.TodoDef{
		Username: tlr.Username,
	}
	// We have to use a faketd here because the list request doesn't use an actual TodoDef
	ts.broadcast("list", &faketd)
	return ts.persistenceLayer.List(tlr)
}

// Listen -
func (ts *TodoServer) Listen(lr *pb.ListenReq, stream pb.Todo_ListenServer) error {
	writing := true
	lis := Listener{
		id:  uuid.New().String(),
		out: make(chan pb.Event),
	}
	ts.addListener(&lis)
	defer ts.delListener(&lis)

	for writing {
		e := <-lis.out

		err := stream.Send(&e)
		if err != nil {
			if err != io.EOF {
				//logs.Error.Printf("problem sending: %v", err)
				return err
			}
			writing = false
		}
	}
	return nil
}

// Run - main entry point for the server.
func (ts *TodoServer) Run() error {
	starttime := time.Now()
	srv := grpc.NewServer()
	pb.RegisterTodoServer(srv, ts)
	listenAddr := ts.config.GetString("server.grpcAddr")
	logs.Debug.Println("listenAddr:", listenAddr)
	if listenAddr == "" {
		logs.Error.Printf("problem listening - no address provided")
	}
	lis, err := net.Listen("tcp", listenAddr)
	if err != nil {
		log.Fatalf("could not listen to %q: %v", listenAddr, err)
	}
	errChan := make(chan error, 10)
	signalChan := make(chan os.Signal, 1)
	signal.Notify(signalChan, syscall.SIGINT, syscall.SIGTERM)

	go func(ec chan error) {
		err := srv.Serve(lis)
		if err != nil {
			ec <- err
			return
		}
	}(errChan)

	for {
		select {
		case err := <-errChan:
			return err

		case s := <-signalChan:
			logs.Debug.Printf("Captured %v, exiting", s)
			logs.Debug.Println("Ran for", time.Now().Sub(starttime).Round(time.Millisecond).String())
			os.Exit(0)
		}
	}
}
