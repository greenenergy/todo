package app

/*
Copyright © 2020 Colin Fox <greenenergy@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

import (
	"encoding/json"
	"fmt"

	"github.com/go-redis/redis/v7"
	pb "gitlab.com/greenenergy/todo/service/todo"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type redisDB struct {
	client *redis.Client
}

// NewRedisDB -
func NewRedisDB(host string, db int) (Persistor, error) {
	client := redis.NewClient(&redis.Options{
		Addr:     host,
		Password: "This_is_a_bad_password!",
		DB:       db,
	})

	return &redisDB{
		client: client,
	}, nil
}

func (rdb *redisDB) Add(td *pb.TodoDef) error {
	js, err := json.Marshal(td)
	if err != nil {
		return err
	}

	refid := fmt.Sprintf("%s:%s", td.Username, td.Id)
	_, err = rdb.client.Set(refid, js, 0).Result()
	return err
}

func (rdb *redisDB) Delete(td *pb.TodoDef) error {
	refid := fmt.Sprintf("%s:%s", td.Username, td.Id)
	_, err := rdb.client.Del(refid).Result()
	return err
}

func (rdb *redisDB) Update(td *pb.TodoDef) (*pb.TodoDef, error) {
	curTd, err := rdb.Get(td)
	if err != nil {
		return nil, err
	}

	if curTd == nil {
		return nil, status.Error(codes.NotFound, "not found")
	}

	if td.Title != "" {
		if td.Title == "." {
			curTd.Title = ""
		} else {
			curTd.Title = td.Title
		}
	}

	if td.State != "" {
		if td.State == "." {
			curTd.State = ""
		} else {
			curTd.State = td.State
		}
	}

	if td.Description != "" {
		if td.Description == "." {
			curTd.Description = ""
		} else {
			curTd.Description = td.Description
		}
	}

	if td.Due != nil {
		curTd.Due = td.Due
	}

	err = rdb.Add(curTd)
	if err != nil {
		return nil, err
	}
	return curTd, nil
}

func (rdb *redisDB) getByKey(key string) (*pb.TodoDef, error) {
	val, err := rdb.client.Get(key).Result()
	if err != nil {
		return nil, err
	}

	var out pb.TodoDef
	err = json.Unmarshal([]byte(val), &out)
	if err != nil {
		return nil, err
	}
	return &out, nil
}

func (rdb *redisDB) Get(td *pb.TodoDef) (*pb.TodoDef, error) {
	refid := fmt.Sprintf("%s:%s", td.Username, td.Id)
	return rdb.getByKey(refid)
}

func (rdb *redisDB) List(td *pb.TodoListReq) (*pb.TodoList, error) {
	keyref := "*"
	if td.Username != "" {
		keyref = td.Username + ":*"
	}

	results, err := rdb.client.Keys(keyref).Result()
	if err != nil {
		return nil, err
	}

	var tdl pb.TodoList

	for _, s := range results {
		td, err := rdb.getByKey(s)
		if err != nil {
			return nil, err
		}

		tdl.Todos = append(tdl.Todos, td)
	}

	return &tdl, nil
}
