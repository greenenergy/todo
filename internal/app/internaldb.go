package app

import (
	"gitlab.com/greenenergy/todo/pkg/logs"
	pb "gitlab.com/greenenergy/todo/service/todo"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

/*
Copyright © 2020 Colin Fox <greenenergy@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

type internalDB struct {
	items map[string]*pb.TodoDef
}

func newInternalDB() *internalDB {
	return &internalDB{
		items: make(map[string]*pb.TodoDef),
	}
}

func (idb *internalDB) Add(td *pb.TodoDef) error {
	idb.items[td.Id] = td
	return nil
}

func (idb *internalDB) Delete(td *pb.TodoDef) {
	delete(idb.items, td.Id)
}

func (idb *internalDB) Get(td *pb.TodoDef) *pb.TodoDef {
	if ctd, ok := idb.items[td.Id]; ok {
		return ctd
	}
	return nil
}

func (idb *internalDB) Update(td *pb.TodoDef) (*pb.TodoDef, error) {

	curTd := idb.Get(td)
	if curTd == nil {
		return nil, status.Error(codes.NotFound, "not found")
	}

	if td.Title != "" {
		if td.Title == "." {
			curTd.Title = ""
		} else {
			curTd.Title = td.Title
		}
	}

	if td.State != "" {
		if td.State == "." {
			curTd.State = ""
		} else {
			curTd.State = td.State
		}
	}

	if td.Description != "" {
		if td.Description == "." {
			curTd.Description = ""
		} else {
			curTd.Description = td.Description
		}
	}

	if td.Due != nil {
		curTd.Due = td.Due
	}

	idb.items[curTd.Id] = curTd
	return curTd, nil
}

type internalMgr struct {
	dbs map[string]*internalDB
}

// NewInternalMgr - allocate a new test db
func NewInternalMgr() Persistor {
	return &internalMgr{
		dbs: make(map[string]*internalDB),
	}
}

func (im *internalMgr) getInternalFor(username string) *internalDB {
	if db, ok := im.dbs[username]; ok {
		return db
	}
	db := newInternalDB()
	im.dbs[username] = db
	return db
}

func (im *internalMgr) List(tlr *pb.TodoListReq) (*pb.TodoList, error) {
	var tdl pb.TodoList

	if tlr.Username == "" {
		logs.Debug.Println("Returning all todos")
		for _, db := range im.dbs {
			for _, tds := range db.items {
				tdl.Todos = append(tdl.Todos, tds)
			}
		}
	} else {
		logs.Debug.Println("Returning todos for", tlr.Username)
		db := im.getInternalFor(tlr.Username)
		for _, tds := range db.items {
			tdl.Todos = append(tdl.Todos, tds)
		}
	}
	return &tdl, nil
}

func (im *internalMgr) Delete(td *pb.TodoDef) error {
	im.getInternalFor(td.Username).Delete(td)
	return nil
}

func (im *internalMgr) Get(td *pb.TodoDef) (*pb.TodoDef, error) {
	return im.getInternalFor(td.Username).Get(td), nil
}

func (im *internalMgr) Add(td *pb.TodoDef) error {
	im.getInternalFor(td.Username).Add(td)
	return nil
}

func (im *internalMgr) Update(td *pb.TodoDef) (*pb.TodoDef, error) {
	db := im.getInternalFor(td.Username)
	return db.Update(td)
}
