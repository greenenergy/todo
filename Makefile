PROJNAME := todo

FILES:=main.go cmd/root.go cmd/server.go cmd/add.go cmd/update.go cmd/delete.go cmd/listen.go cmd/list.go \
	service/$(PROJNAME)/$(PROJNAME).pb.go internal/app/server.go \
	internal/app/internaldb.go internal/app/redisdb.go

td: $(FILES)
	CGO_ENABLED=0 GOOS=linux go build -ldflags "-s -w -extldflags '-static' -X main.githash=$(FILEGITHASH)" -o td -a -installsuffix cgo

service/$(PROJNAME)/$(PROJNAME).pb.go: service/$(PROJNAME)/$(PROJNAME).proto
	@protoc -I service/$(PROJNAME) service/$(PROJNAME)/$(PROJNAME).proto --go_out=plugins=grpc:service/$(PROJNAME)

docker: td
	docker build -t greenenergy/$(PROJNAME):latest .

