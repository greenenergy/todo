package cmd

/*
Copyright © 2020 Colin Fox <greenenergy@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

import (
	"context"
	"encoding/json"
	"io"
	"log"

	"github.com/spf13/cobra"
	"gitlab.com/greenenergy/todo/pkg/logs"
	pb "gitlab.com/greenenergy/todo/service/todo"
)

// listenCmd represents the listen command
var listenCmd = &cobra.Command{
	Use:   "listen",
	Short: "Opens a stream listener which will report on all todo activity",
	Long: `This command will start a stream listener
that monitors all todo actions and prints them to the
console. This will report *all* actions, not just those
of the current user`,
	Run: func(cmd *cobra.Command, args []string) {
		client, err := GetClient()
		if err != nil {
			log.Fatal(err.Error())
		}

		var lr pb.ListenReq
		ctx := context.Background()
		stream, err := client.Listen(ctx, &lr)

		reading := true
		for reading {
			e, err := stream.Recv()
			if err != nil {
				if err != io.EOF {
					logs.Error.Printf("problem reading: %v", err)
					return
				}
				reading = false
			} else {
				o, _ := json.MarshalIndent(e, "", "    ")
				logs.Info.Println(string(o))
			}
		}

	},
}

func init() {
	rootCmd.AddCommand(listenCmd)
}
