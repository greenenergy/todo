package cmd

/*
Copyright © 2020 Colin Fox <greenenergy@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

import (
	"log"

	"github.com/spf13/cobra"
	"gitlab.com/greenenergy/todo/internal/app"
)

// serverCmd represents the server command
var serverCmd = &cobra.Command{
	Use:   "server",
	Short: "Runs the todo server",
	Long: `This is the command that runs the todo server,
and acts as the entrypoint in the docker image.`,
	Run: func(cmd *cobra.Command, args []string) {

		// The InternalMgr was used during development as a simple, mock persistence engine.
		//persist := app.NewInternalMgr()

		redisHost := config.GetString("server.redis")
		persist, err := app.NewRedisDB(redisHost, 0)

		if err != nil {
			log.Fatal(err.Error())
		}
		s := app.NewTodoServer(config, persist)
		s.Run()
	},
}

func init() {
	rootCmd.AddCommand(serverCmd)
}
