package cmd

/*
Copyright © 2020 Colin Fox <greenenergy@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

import (
	"context"
	"encoding/json"
	"log"
	"os"
	"time"

	"github.com/golang/protobuf/ptypes"
	"github.com/golang/protobuf/ptypes/timestamp"
	"gitlab.com/greenenergy/todo/pkg/logs"
	pb "gitlab.com/greenenergy/todo/service/todo"

	"github.com/spf13/cobra"
)

// addCmd represents the add command
var addCmd = &cobra.Command{
	Use:   "add <title>",
	Short: "Adds a todo item",
	Long: `This command adds a new todo item. You may enter
just the title, or you may also add a description, due date and state`,
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) < 1 {
			cmd.Usage()
			os.Exit(1)
		}

		title := args[0]

		client, err := GetClient()
		if err != nil {
			log.Fatal(err.Error())
		}
		username, err := Username()
		if err != nil {
			log.Fatal(err.Error())
		}

		// We're using the Golang time package to parse the
		// string, and then we need a helper to convert it to a
		// protobuf friendly format.
		// Time format supported is: "2006-01-02T15:04:05Z07:00"
		var ts *timestamp.Timestamp
		dueStr := cmd.Flags().Lookup("due").Value.String()
		if dueStr != "" {
			t, err := time.Parse(time.RFC3339, dueStr)
			if err != nil {
				log.Fatal(err.Error())
			}
			ts, err = ptypes.TimestampProto(t)
			if err != nil {
				log.Fatal(err.Error())
			}
		}

		state := "todo"
		if s := cmd.Flags().Lookup("state").Value.String(); s != "" {
			state = s
		}

		td := pb.TodoDef{
			Title:       title,
			Username:    username,
			Description: cmd.Flags().Lookup("description").Value.String(),
			State:       state,
			Due:         ts,
		}

		ctx := context.Background()
		res, err := client.Add(ctx, &td)
		if err != nil {
			log.Fatal(err.Error())
		}

		t, _ := json.MarshalIndent(res, "", "    ")
		logs.Info.Println(string(t))
	},
}

func init() {
	rootCmd.AddCommand(addCmd)

	addCmd.Flags().StringP("description", "d", "", "Description of the item.")
	addCmd.Flags().StringP("due", "D", "", "When the item is due. Format: 2006-01-02T15:04:05Z07:00 ")
	addCmd.Flags().StringP("state", "s", "", "State the item is in - todo, prog, done")
}
