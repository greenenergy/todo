package cmd

/*
Copyright © 2020 Colin Fox <greenenergy@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

import (
	"context"
	"encoding/json"
	"log"

	"gitlab.com/greenenergy/todo/pkg/logs"
	pb "gitlab.com/greenenergy/todo/service/todo"

	"github.com/spf13/cobra"
)

// listCmd represents the list command
var listCmd = &cobra.Command{
	Use:   "list",
	Short: "Lists todo items.",
	Long: `This command lists todo items.
By default, it lists only the user's own items, but
there is an "all" commandline flag which will list
them all.`,
	Run: func(cmd *cobra.Command, args []string) {
		client, err := GetClient()
		if err != nil {
			log.Fatal(err.Error())
		}
		username, err := Username()
		if err != nil {
			log.Fatal(err.Error())
		}

		all := cmd.Flags().Lookup("all").Value.String()
		if all == "true" {
			username = ""
		}

		tlr := pb.TodoListReq{
			Username: username,
		}
		ctx := context.Background()
		res, err := client.ListTodos(ctx, &tlr)
		if err != nil {
			log.Fatal(err.Error())
		}

		t, _ := json.MarshalIndent(res, "", "    ")
		logs.Info.Println(string(t))
	},
}

func init() {
	rootCmd.AddCommand(listCmd)

	listCmd.Flags().BoolP("all", "a", false, "list all items instead of just your own")
}
