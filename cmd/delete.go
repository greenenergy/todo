package cmd

/*
Copyright © 2020 Colin Fox <greenenergy@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

import (
	"context"
	"encoding/json"
	"log"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/greenenergy/todo/pkg/logs"
	pb "gitlab.com/greenenergy/todo/service/todo"
)

// deleteCmd represents the delete command
var deleteCmd = &cobra.Command{
	Use:   "delete <id>",
	Short: "Deletes the indicated todo item",
	Long:  ``,
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) < 1 {
			cmd.Usage()
			os.Exit(1)
		}

		client, err := GetClient()
		if err != nil {
			log.Fatal(err.Error())
		}
		username, err := Username()
		if err != nil {
			log.Fatal(err.Error())
		}

		td := pb.TodoDef{
			Username: username,
			Id:       args[0],
		}
		ctx := context.Background()
		res, err := client.Delete(ctx, &td)
		if err != nil {
			log.Fatal(err.Error())
		}

		t, _ := json.MarshalIndent(res, "", "    ")
		logs.Info.Println(string(t))

	},
}

func init() {
	rootCmd.AddCommand(deleteCmd)
}
