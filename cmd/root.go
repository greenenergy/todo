package cmd

/*
Copyright © 2020 Colin Fox <greenenergy@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

import (
	"fmt"
	"log"
	"os"
	"os/user"

	"github.com/spf13/cobra"
	"gitlab.com/greenenergy/todo/pkg/logs"
	pb "gitlab.com/greenenergy/todo/service/todo"
	"google.golang.org/grpc"

	"github.com/spf13/viper"
)

var (
	config *viper.Viper
)

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "td",
	Short: "todo client & server",
	Long:  `todo is a simple client/server combination that lets users manage todo lists, using redis as the backing store.`,
	// Uncomment the following line if your bare application
	// has an action associated with it:
	//	Run: func(cmd *cobra.Command, args []string) { },
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func init() {
	cobra.OnInitialize(initConfig)
}

// GetClient - return a connected client
func GetClient() (pb.TodoClient, error) {
	listenAddr := config.GetString("server.grpcAddr")
	conn, err := grpc.Dial(listenAddr, grpc.WithInsecure())
	if err != nil {
		return nil, fmt.Errorf("problem connecting to grpc: %w", err)
	}
	client := pb.NewTodoClient(conn)
	return client, nil
}

// Username - return the unix username of the current user
func Username() (string, error) {
	u, err := user.Current()
	if err != nil {
		return "", err
	}
	return u.Username, nil
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	config = viper.New()
	config.SetConfigName("todo")
	config.SetConfigType("toml")
	config.AddConfigPath("/etc")
	config.AddConfigPath("etc")
	config.AddConfigPath(".")
	err := config.ReadInConfig()
	if err != nil {
		logs.Error.Println("Config file used:", config.ConfigFileUsed())
		logs.Error.Println("problem with config file:", err.Error())
		os.Exit(-1)
	}

	err = config.BindPFlags(rootCmd.PersistentFlags())
	if err != nil {
		log.Fatal(err.Error())
	}

	grpcListen := config.GetString("server.grpcAddr")
	if grpcListen == "" {
		log.Fatal("no grpcAddr in server section")
	}
}
