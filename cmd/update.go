package cmd

/*
Copyright © 2020 Colin Fox <greenenergy@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

import (
	"context"
	"encoding/json"
	"log"
	"os"
	"time"

	"github.com/golang/protobuf/ptypes"
	"github.com/golang/protobuf/ptypes/timestamp"
	"github.com/spf13/cobra"
	"gitlab.com/greenenergy/todo/pkg/logs"
	pb "gitlab.com/greenenergy/todo/service/todo"
)

// updateCmd represents the update command
var updateCmd = &cobra.Command{
	Use:   "update <id>",
	Short: "Update the indicated todo item",
	Long: `This command allows you to change any of 
the values of one of your todo items. For this simple
demo, we're using an empty string to imply an unchanged
value, so if you want to clear a field, use the placeholder
"." and the server will clear it.`,
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) < 1 {
			cmd.Usage()
			os.Exit(1)
		}

		id := args[0]

		client, err := GetClient()
		if err != nil {
			log.Fatal(err.Error())
		}
		username, err := Username()
		if err != nil {
			log.Fatal(err.Error())
		}

		// We're using the Golang time package to parse the
		// string, and then we need a helper to convert it to a
		// protobuf friendly format.
		// Time format supported is: "2006-01-02T15:04:05Z07:00"
		var ts *timestamp.Timestamp
		dueStr := cmd.Flags().Lookup("due").Value.String()
		if dueStr != "" {
			if dueStr == "." {
				ts = &timestamp.Timestamp{}
			} else {
				t, err := time.Parse(time.RFC3339, dueStr)
				if err != nil {
					log.Fatal(err.Error())
				}
				ts, err = ptypes.TimestampProto(t)
				if err != nil {
					log.Fatal(err.Error())
				}
			}
		}

		td := pb.TodoDef{
			Id:          id,
			Title:       cmd.Flags().Lookup("title").Value.String(),
			Username:    username,
			Description: cmd.Flags().Lookup("description").Value.String(),
			State:       cmd.Flags().Lookup("state").Value.String(),
			Due:         ts,
		}

		ctx := context.Background()
		res, err := client.Update(ctx, &td)
		if err != nil {
			log.Fatal(err.Error())
		}

		t, _ := json.MarshalIndent(res, "", "    ")
		logs.Info.Println(string(t))
	},
}

func init() {
	rootCmd.AddCommand(updateCmd)

	updateCmd.Flags().StringP("description", "d", "", "Description of the item.")
	updateCmd.Flags().StringP("due", "D", "", "When the item is due. Format: 2006-01-02T15:04:05Z07:00 ")
	updateCmd.Flags().StringP("state", "s", "", "State the item is in - todo, prog, done")
	updateCmd.Flags().StringP("title", "t", "", "The title")
}
