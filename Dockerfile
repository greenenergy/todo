FROM alpine:latest
RUN apk --no-cache add ca-certificates
COPY td /td
ENTRYPOINT ["/td", "server"]

