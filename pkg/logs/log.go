package logs

import (
	"bytes"
	"fmt"
	"io"
	"log"
	"os"
	"time"
)

var (
	Info     *log.Logger
	Error    *log.Logger
	TeeError *log.Logger
	Debug    *log.Logger
	Warn     *log.Logger
	logs     *LogSet
)

func init() {
	logs = NewLogSet()
	logs.Defaults(os.Stdout)
	Info = logs.Info
	Error = logs.Error
	TeeError = logs.TeeError
	Debug = logs.Debug
	Warn = logs.Warn
}

// Out - the output accumulator.
type Out struct {
	s fmt.Stringer
	w io.Writer
}

func (o Out) String() string {
	return o.s.String()
}

func (o *Out) Write(input []byte) (int, error) {
	var b bytes.Buffer

	b.Write([]byte(o.String()))
	b.Write(input)

	return o.w.Write(b.Bytes())
}

func WrapOut(w io.Writer, s fmt.Stringer) io.Writer {
	return &Out{s: s, w: w}
}

type TimeStruct struct{}

var (
	customTimeFormat = "2006-01-02T15:04:05.000Z07:00"
)

func (ts TimeStruct) String() string {
	return time.Now().Format(customTimeFormat)
}

type LogSet struct {
	Info     *log.Logger
	Error    *log.Logger // Error still just sends to Stdout
	TeeError *log.Logger // TeeError sends to both Stdout and Stderr
	Debug    *log.Logger
	Warn     *log.Logger
}

func NewLogSet() *LogSet {
	return &LogSet{}
}

func (ls *LogSet) Defaults(out io.Writer) {
	if out == nil {
		out = os.Stdout
	}
	std_prefix := WrapOut(out, TimeStruct{})
	err_prefix := WrapOut(io.MultiWriter(out, os.Stderr), TimeStruct{})

	ls.Warn = log.New(std_prefix, " [ WARN] ", log.Lshortfile)
	ls.Debug = log.New(std_prefix, " [DEBUG] ", log.Lshortfile)
	ls.Info = log.New(std_prefix, " [ INFO] ", 0)
	ls.Error = log.New(std_prefix, " [ERROR] ", log.Llongfile)
	ls.TeeError = log.New(err_prefix, " [ERROR] ", log.Llongfile)
}
