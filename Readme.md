# Todo

This is a simple todo list server that runs in a docker-compose session, using Redis for persistence.

Prerequisites to building are: Go v1.14, make, docker.

## Overview

To build, just do:

    make docker
  
This will compile the project and then build & install the docker image, which you will need for docker-compose.

The project uses go modules, and will automatically install any necessary go packages at build time.

To launch the docker-compose environment, do:

    cd testing
    docker-compose up

This will start docker-compose and launch both the todo server and redis.

## Usage

Now you can use the `td` executable to work with your todo items. Executing `./td -h` will show help, including the subcommands and flags. There is only one executable, and it `./td server` launches it in server mode, otherwise it acts as a client.

One client command is special: `./td listen`. This will put the command into listen mode, and it will report on all actions executed on the server. CTRL-C to stop it.

The config file is `etc/todo.toml`, and the defaults will work for your current machine. If you wish to talk to a todo server running on a different machine, you will need to update the config file and set the `grpcAddr` to your target machine.

The `td` in server mode also uses the `grpcAddr` as a listen address, so you can change the port there if the default one is already taken.
